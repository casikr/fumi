jQuery(function($) {
    $(document).ready(function() {
        $("#slide").slick({
          infinite: true,
          slidesToShow: 1,
          slidesToScroll: 1,
          autoplay: true,
          autoplaySpeed: 3000,
          responsive: [
              {
                breakpoint: 1024,
                settings: {
                  adaptiveHeight: true,
                }
            }],

          nextArrow: '<span class="icoRight">></span>',
          prevArrow: '<span class="icoLeft"><</span>',
        });
        var h = $("#mainHeader").height();
        $('.menu').onePageNav({
            currentClass: 'current',
            changeHash: false,
            scrollOffset: h,
            scrollSpeed: 750
        });

        $("#ham").click(function() {
            $(".menu").slideToggle();
        });





        function getDots() {
            var responsive_viewport = window.innerWidth || $(window).width();
            var hd = document.getElementById('headDot');
            var getHd = hd.getBoundingClientRect();
            var dc = document.getElementById('dotContainer');
            if(dc) {

                var s = dc.getBoundingClientRect();
                var t = document.querySelector('.dotys').style.left = "-" + s.left + "px";
            }
            if(hd) {

                $("#headDot:after").css("width", responsive_viewport - getHd.right + "px");
                console.log(responsive_viewport - getHd.right + "px");
            }
        }
      
        getDots();
        $(window).on("resize", function() {
            getDots();
        });
    });
});
