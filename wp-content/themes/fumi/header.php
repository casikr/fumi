<!DOCTYPE html>
<!--[if IE 7]>
<html class="ie ie7" <?php language_attributes(); ?>>
<![endif]-->
<!--[if IE 8]>
<html class="ie ie8" <?php language_attributes(); ?>>
<![endif]-->
<!--[if !(IE 7) | !(IE 8)  ]><!-->
<html <?php language_attributes(); ?>>
<!--<![endif]-->
<head>
<meta charset="<?php bloginfo( 'charset' ); ?>">
<!--[if 9]><meta content='IE=edge' http-equiv='X-UA-Compatible'/><![endif]-->
<meta name="viewport" content="width=device-width, initial-scale=1.0">
<title><?php wp_title('-'); ?></title>

<?php wp_head(); ?>
<!--[if lt IE 9]>
<script src="<?php echo get_template_directory_uri(); ?>/js/html5shiv.min.js"></script>
<![endif]-->
</head>
<body <?php body_class(); ?>>
    <header id="mainHeader">
        <div class="container">
            <nav id="mainNav">
                <a href="<?php echo site_url('/'); ?>">
                    <img src="<?php echo THE_URI; ?>/img/fumi.png" alt="FUMI" />
                </a>
                <?php wp_nav_menu(array("theme_location" => "header-menu", "container" => "")); ?>
                <img id="ham" src="<?php echo get_stylesheet_directory_uri(); ?>/img/arrows_hamburger.svg" alt="ham">
            </nav>
        </div>
    </header>
