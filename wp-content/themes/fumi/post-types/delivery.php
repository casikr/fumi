<?php

function delivery_init() {
	register_post_type( 'delivery', array(
		'labels'            => array(
			'name'                => __( 'Deliveries', 'fumi' ),
			'singular_name'       => __( 'Delivery', 'fumi' ),
			'all_items'           => __( 'All Deliveries', 'fumi' ),
			'new_item'            => __( 'New delivery', 'fumi' ),
			'add_new'             => __( 'Add New', 'fumi' ),
			'add_new_item'        => __( 'Add New delivery', 'fumi' ),
			'edit_item'           => __( 'Edit delivery', 'fumi' ),
			'view_item'           => __( 'View delivery', 'fumi' ),
			'search_items'        => __( 'Search deliveries', 'fumi' ),
			'not_found'           => __( 'No deliveries found', 'fumi' ),
			'not_found_in_trash'  => __( 'No deliveries found in trash', 'fumi' ),
			'parent_item_colon'   => __( 'Parent delivery', 'fumi' ),
			'menu_name'           => __( 'Deliveries', 'fumi' ),
		),
		'public'            => true,
		'hierarchical'      => false,
		'show_ui'           => true,
		'show_in_nav_menus' => true,
		'supports'          => array( 'title', 'editor' ),
		'has_archive'       => true,
		'rewrite'           => true,
		'query_var'         => true,
		'menu_icon'         => 'dashicons-admin-post',
		'show_in_rest'      => true,
		'rest_base'         => 'delivery',
		'rest_controller_class' => 'WP_REST_Posts_Controller',
	) );

}
add_action( 'init', 'delivery_init' );

function delivery_updated_messages( $messages ) {
	global $post;

	$permalink = get_permalink( $post );

	$messages['delivery'] = array(
		0 => '', // Unused. Messages start at index 1.
		1 => sprintf( __('Delivery updated. <a target="_blank" href="%s">View delivery</a>', 'fumi'), esc_url( $permalink ) ),
		2 => __('Custom field updated.', 'fumi'),
		3 => __('Custom field deleted.', 'fumi'),
		4 => __('Delivery updated.', 'fumi'),
		/* translators: %s: date and time of the revision */
		5 => isset($_GET['revision']) ? sprintf( __('Delivery restored to revision from %s', 'fumi'), wp_post_revision_title( (int) $_GET['revision'], false ) ) : false,
		6 => sprintf( __('Delivery published. <a href="%s">View delivery</a>', 'fumi'), esc_url( $permalink ) ),
		7 => __('Delivery saved.', 'fumi'),
		8 => sprintf( __('Delivery submitted. <a target="_blank" href="%s">Preview delivery</a>', 'fumi'), esc_url( add_query_arg( 'preview', 'true', $permalink ) ) ),
		9 => sprintf( __('Delivery scheduled for: <strong>%1$s</strong>. <a target="_blank" href="%2$s">Preview delivery</a>', 'fumi'),
		// translators: Publish box date format, see http://php.net/date
		date_i18n( __( 'M j, Y @ G:i' ), strtotime( $post->post_date ) ), esc_url( $permalink ) ),
		10 => sprintf( __('Delivery draft updated. <a target="_blank" href="%s">Preview delivery</a>', 'fumi'), esc_url( add_query_arg( 'preview', 'true', $permalink ) ) ),
	);

	return $messages;
}
add_filter( 'post_updated_messages', 'delivery_updated_messages' );
