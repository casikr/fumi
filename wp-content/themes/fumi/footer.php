        <footer id="mainFooter">
            <div class="container">
                <div class="row">
                    <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
                        <footer>
                            <h5><?php the_field('footer_contact_title', 'option'); ?></h5>
                            <?php the_field("footer_contact", "option"); ?>
                            <?php $social_media = get_field("social_media", "option");
                            if($social_media) { ?>
                                <ul class="socialMedia">
                                    <?php
                                    foreach($social_media as $s) { ?>
                                        <li><a href="<?php echo esc_url($s['link']); ?>"><img src="<?php echo esc_url($s['icon']['url']); ?>" alt="<?php echo $s['icon']['alt']; ?>"></a></li>

                                    <?php } ?>
                                </ul>

                            <?php } ?>
                        </footer>
                    </div>
                    <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
                        <footer>
                        <h5><?php the_field('footer_newsletter_title', 'option'); ?></h5>
                        <?php the_field("footer_newsletter", "option"); ?>
                        <form method="post" action="https://app.freshmail.com/pl/actions/subscribe/"><br />
                        <input type="hidden" name="subscribers_list_hash" value="ku6k8ws5ot" />
                        <input type="text" id="freshmail_email" name="freshmail_email" placeholder="Email"/>
                        <button type="submit"/>Zapisz się</button>
                        </form>
                        </footer>
                    </div>

                </div>

            </div>

        </footer>
        <?php wp_footer();?>
    </body>
</html>
