<?php if ( have_posts() ) : while ( have_posts() ) : the_post(); ?>
    <!-- post -->


    <section id="about">
        <div class="container">
            <div class="aboutText">
                <div class="row">
                    <div class="col-lg-10 col-lg-offset-1 col-md-10 col-md-offset-1 col-sm-10 col-sm-offset-1 col-xs-12">
                        <div class="innerAbout">
                            <?php the_field('about_us'); ?>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>

<?php endwhile; ?>
    <!-- post navigation -->
<?php else: ?>
    <!-- no posts found -->
<?php endif; ?>
<section id="proces">
    <div class="container">
        <div class="row">
            <div class="col-lg-10 col-lg-offset-1 col-md-10 col-md-offset-1 col-sm-12 col-xs-12">
                <h2 id="headDot">Proces<br /> produkcji</h2>
                <div id="dotContainer">
                    <div class="dotys">

                    </div>
                </div>
                <div class="row">
                    <?php query_posts(array('post_type' => 'proces')); ?>
                    <?php $counter = 0; ?>
                    <?php if ( have_posts() ) : while ( have_posts() ) : the_post(); ?>
                        <?php $counter ++; ?>
                        <!-- post -->
                        <div class="col-lg-4 col-md-4 col-sm-12 col-xs-12">
                            <div class="circle">
                                <?php echo $counter; ?>
                            </div>
                            <h3><?php the_title(); ?></h3>
                            <?php the_content(); ?>
                        </div>
                    <?php endwhile; ?>
                        <?php $counter = null; ?>
                        <!-- post navigation -->
                    <?php else: ?>
                        <!-- no posts found -->
                    <?php endif; ?>
                    <?php wp_reset_query(); ?>
                </div>
            </div>
        </div>
    </div>
</section>
<section id="products">
    <div class="container">
        <div class="row">
            <div id="slide" class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                <?php query_posts(array('post_type' => 'product')); ?>
                <?php if ( have_posts() ) : while ( have_posts() ) : the_post(); ?>
                    <!-- post -->


                        <article class="prod">
                            <div style="background-image: url(<?php the_post_thumbnail_url('full'); ?>);" class="bgr">
                                <?php the_post_thumbnail('full', array('class' => 'img-responsive')); ?>
                            </div>
                            <div class="layerText">
                                <h3><?php the_title(); ?></h3>
                                <?php the_excerpt(); ?>
                            </div>
                            <div class="layerContent">
                                <dl class="ingredients">
                                    <dt>Skład</dt>

                                <?php $ingredients = get_field('ingredients');

                                if($ingredients) {
                                echo '<dd>';
                                foreach ($ingredients as $value) {
                                    # code...
                                    echo '<span>' . $value['item'] . '</span>';
                                }
                                echo '</dd>';
                                }


                                ?>
                                <?php $meat = get_field('meat');
                                if($meat) {
                                    echo '<dt id="ico">';
                                    foreach($meat as $m) {
                                        if($m == "krowa") { ?>
                                            <img class="img-responsive center-block" src="<?php echo get_stylesheet_directory_uri(); ?>/img/cow.png" alt="wołowina">
                                        <?php } elseif($m == "kurczak") { ?>
                                            <img class="img-responsive center-block" src="<?php echo get_stylesheet_directory_uri(); ?>/img/chicken.png" alt="kurczak">
                                        <?php } elseif($m == "indyk") { ?>
                                            <img class="img-responsive center-block" src="<?php echo get_stylesheet_directory_uri(); ?>/img/turky.png" alt="Indyk">
                                        <?php }
                                    }
                                    echo '</dt>';
                                }


                                ?>

                                </dl>
                            </div>

                        </article>


                <?php endwhile; ?>
                    <!-- post navigation -->
                <?php else: ?>
                    <!-- no posts found -->
                <?php endif; ?>
                <?php wp_reset_query(); ?>

            </div>
        </div>
    </div>
</section>
<section id="delivery">
    <div class="container">
        <div class="centki">
            <div class="row">

            </div>
        </div>
    </div>
    <div class="container sweet">

        <div class="row">
            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                <?php if ( have_posts() ) : while ( have_posts() ) : the_post(); ?>
                    <!-- post -->
                    <div class="graf">
                        <?php $delivery = get_field('delivery');
                        if($delivery) {
                            $c = 0;
                            $out = '';
                            foreach ($delivery as $v) {


                                $out .= '<figure>';
                                    $out .= '<img class="center-block" src="' . esc_url($v['ikona']['url']). '" alt="' . $v['ikona']['alt'] . '" />';
                                    $out .= '<figcaption>';
                                        $out .= $v['tekst'];
                                    $out .= '</figcaption>';

                                $out .= '</figure>';

                                # code...
                            }
                            echo $out;
                        }




                        ?>

                    </div>
                    <h3>Dostawa</h3>
                <?php endwhile; ?>
                    <!-- post navigation -->
                <?php else: ?>
                    <!-- no posts found -->
                <?php endif; ?>
            </div>

        </div>

    </div>

</section>
