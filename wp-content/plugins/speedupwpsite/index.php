<?php
/*
Plugin Name: SpeedUp WP Site Plugin
Plugin URI: http://speedupwpsite.com/
Description: A one click solution to speeding up your wordpress site. No Setup needed. 
Author: speedupwpsite
Author URI: http://speedupwpsite.com/
Version: 1.0.0  
*/

require_once('wpsb-function.php');
/*================= speedup wp site Htaccess ===========*/
global $wpsb;
global $wpsblink;
global $wpsb1;
global $wpsb2;
global $wpsb3;
global $wpsb4;
global $wpsb5;
global $wpsb6;

$wpsburl = strtolower(get_bloginfo('url'));
$wpsburl = str_replace('https://','',$wpsburl);
$wpsburl = str_replace('http://','',$wpsburl);
$wpsburl = str_replace('www.','',$wpsburl);

$wpsblink = "
# speedup wp site Plugin Starts

RewriteEngine on
RewriteCond %{HTTP_REFERER} !^$
RewriteCond %{HTTP_REFERER} !^http(s)?://(www\.)?".$wpsburl." [NC]
RewriteCond %{HTTP_REFERER} !^http(s)?://(www\.)?google.com [NC]
RewriteRule \.(jpg|jpeg|png|gif)$ - [NC,F,L]
 
";

	$wpsb1 .= '# GZIP '."\r\n"."\r\n";
	$wpsb1 .= '<IfModule mod_deflate.c>'."\r\n";
	$wpsb1 .= 'AddOutputFilterByType DEFLATE text/plain'."\r\n";
	$wpsb1 .= 'AddOutputFilterByType DEFLATE text/html'."\r\n";
	$wpsb1 .= 'AddOutputFilterByType DEFLATE text/xml'."\r\n";
	$wpsb1 .= 'AddOutputFilterByType DEFLATE text/css'."\r\n";
	$wpsb1 .= 'AddOutputFilterByType DEFLATE text/javascript'."\r\n";
	$wpsb1 .= 'AddOutputFilterByType DEFLATE application/xml'."\r\n";
	$wpsb1 .= 'AddOutputFilterByType DEFLATE application/xhtml+xml'."\r\n";
	$wpsb1 .= 'AddOutputFilterByType DEFLATE application/rss+xml'."\r\n";
	$wpsb1 .= 'AddOutputFilterByType DEFLATE application/javascript'."\r\n";
	$wpsb1 .= 'AddOutputFilterByType DEFLATE application/x-javascript'."\r\n";
	$wpsb1 .= 'AddOutputFilterByType DEFLATE application/x-httpd-php'."\r\n";
	$wpsb1 .= 'AddOutputFilterByType DEFLATE application/x-httpd-fastphp'."\r\n";
	$wpsb1 .= 'AddOutputFilterByType DEFLATE application/vnd.ms-fontobject'."\r\n";
	$wpsb1 .= 'AddOutputFilterByType DEFLATE application/x-font'."\r\n";
	$wpsb1 .= 'AddOutputFilterByType DEFLATE application/x-font-opentype'."\r\n";
	$wpsb1 .= 'AddOutputFilterByType DEFLATE application/x-font-otf'."\r\n";
	$wpsb1 .= 'AddOutputFilterByType DEFLATE application/x-font-truetype'."\r\n";
	$wpsb1 .= 'AddOutputFilterByType DEFLATE application/x-font-ttf'."\r\n";
	$wpsb1 .= 'AddOutputFilterByType DEFLATE image/svg+xml'."\r\n";
	$wpsb1 .= 'AddOutputFilterByType DEFLATE image/x-icon'."\r\n";
	$wpsb1 .= 'AddOutputFilterByType DEFLATE font/opentype'."\r\n";
	$wpsb1 .= 'AddOutputFilterByType DEFLATE font/otf'."\r\n";
	$wpsb1 .= 'AddOutputFilterByType DEFLATE font/ttf'."\r\n";
	$wpsb1 .= 'BrowserMatch ^Mozilla/4 gzip-only-text/html'."\r\n";
	$wpsb1 .= 'BrowserMatch ^Mozilla/4\.0[678] no-gzip'."\r\n";
	$wpsb1 .= 'BrowserMatch \bMSI[E] !no-gzip !gzip-only-text/html'."\r\n";
	$wpsb1 .= 'Header append Vary User-Agent env=!dont-vary'."\r\n";
	$wpsb1 .= '</IfModule>'."\r\n"."\r\n";

	$wpsb2 .= '# Leverage Browser Caching Start'."\r\n"."\r\n";
	$wpsb2 .= '<IfModule mod_expires.c>'."\r\n";
	$wpsb2 .= 'ExpiresActive On'."\r\n";
	$wpsb2 .= 'ExpiresByType image/jpg "access 1 year"'."\r\n";
	$wpsb2 .= 'ExpiresByType image/jpeg "access 1 year"'."\r\n";
	$wpsb2 .= 'ExpiresByType image/gif "access 1 year"'."\r\n";
	$wpsb2 .= 'ExpiresByType image/png "access 1 year"'."\r\n";
	$wpsb2 .= 'ExpiresByType text/css "access 1 month"'."\r\n";
	$wpsb2 .= 'ExpiresByType text/html "access 1 month"'."\r\n";
	$wpsb2 .= 'ExpiresByType application/pdf "access 1 month"'."\r\n";
	$wpsb2 .= 'ExpiresByType application/javascript "access plus 1 year"'."\r\n";
	$wpsb2 .= 'ExpiresByType text/x-javascript "access 1 month"'."\r\n";
	$wpsb2 .= 'ExpiresByType application/x-shockwave-flash "access 1 month"'."\r\n";
	$wpsb2 .= 'ExpiresByType image/x-icon "access 1 year"'."\r\n";
	$wpsb2 .= 'ExpiresDefault "access 1 month"'."\r\n";
	$wpsb2 .= '</IfModule>'."\r\n"."\r\n";
	
	$wpsb3 .= '# Caching of common files Start'."\r\n"."\r\n";
	$wpsb3 .= '<IfModule mod_headers.c>'."\r\n";
	$wpsb3 .= '<FilesMatch "\.(ico|pdf|flv|swf|js|css|gif|png|jpg|jpeg|ico|txt|html|htm)$">'."\r\n";
	$wpsb3 .= 'Header set Cache-Control "max-age=2592000, public"'."\r\n";
	$wpsb3 .= '</FilesMatch>'."\r\n";
	$wpsb3 .= '</IfModule>'."\r\n"."\r\n";

	$wpsb4 .= '# Enable Keepalive Start'."\r\n"."\r\n";
	$wpsb4 .= '<ifModule mod_headers.c>'."\r\n";
	$wpsb4 .= 'Header set Connection keep-alive'."\r\n";
	$wpsb4 .= '</ifModule>'."\r\n"."\r\n";
	
	$wpsb5 .= '# Use UTF-8 encoding Start'."\r\n"."\r\n";
	$wpsb5 .= 'AddDefaultCharset utf-8'."\r\n"."\r\n";
	
	$wpsb6 .= '# Enable Vary: Accept-Encoding Start'."\r\n"."\r\n";
	$wpsb6 .= '<IfModule mod_headers.c>'."\r\n";
	$wpsb6 .= '<FilesMatch "\.(js|css|xml|gz)$">'."\r\n";
	$wpsb6 .= 'Header append Vary: Accept-Encoding'."\r\n";
	$wpsb6 .= '</FilesMatch>'."\r\n";
	$wpsb6 .= '</IfModule>'."\r\n"."\r\n";

	$wpsb6 .= '# speedup wp site Plugin Ends'."\r\n"."\r\n";

$wpsb = ABSPATH.'.htaccess';
/*=========== speedup wp site Activate =================*/
function wpsb_activate() {
	global $wpsb;
	global $wpsblink;
	global $wpsb1;
	global $wpsb2;
	global $wpsb3;
	global $wpsb4;
	global $wpsb5;
	global $wpsb6;

	if (file_exists($wpsb)) {
		$bs = fopen($wpsb, 'r');
		$wpsbaccess = fread($bs, filesize($wpsb));
		fclose($bs);
  	}
	$bs = fopen($wpsb, 'w') or die("can't open file");
	fwrite($bs, $wpsbaccess.$wpsblink);
	fwrite($bs, $wpsb1);
	fwrite($bs, $wpsb2);
	fwrite($bs, $wpsb3);
	fwrite($bs, $wpsb4);
	fwrite($bs, $wpsb5);
	fwrite($bs, $wpsb6);
	fclose($bs);
}
register_activation_hook( __FILE__, 'wpsb_activate' );
 
/*========== speedup wp site Deactivate ============*/
function wpsb_deactivate() {
	global $wpsb;
	global $wpsblink;
	global $wpsb1;
	global $wpsb2;
	global $wpsb3;
	global $wpsb4;
	global $wpsb5;
	global $wpsb6;

	if (file_exists($wpsb)) {
		$bs = fopen($wpsb, 'r');
		$wpsbaccess = fread($bs, filesize($wpsb));
		fclose($bs);
		$wpsbaccess = str_replace($wpsblink, "", $wpsbaccess);
		$wpsbaccess = str_replace($wpsb1, "",$wpsbaccess);
		$wpsbaccess = str_replace($wpsb2, "",$wpsbaccess);
		$wpsbaccess = str_replace($wpsb3, "",$wpsbaccess);
		$wpsbaccess = str_replace($wpsb4, "",$wpsbaccess);
		$wpsbaccess = str_replace($wpsb5, "",$wpsbaccess);
		$wpsbaccess = str_replace($wpsb6, "",$wpsbaccess);
		$bs = fopen($wpsb, 'w') or die("can't open file");
		fwrite($bs, $wpsbaccess);
		fclose($bs);
	}
}
register_deactivation_hook( __FILE__, 'wpsb_deactivate' );