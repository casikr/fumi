<?php
/*========== speedup wp site Remove Css Js ===========*/ 
function wpsb_remove_css_js( $src ) {
	if ( strpos( $src, 'ver=' ) ) {
		$src = remove_query_arg( 'ver', $src );
	}
	return $src;
}
add_filter( 'style_loader_src', 'wpsb_remove_css_js', 9999 );
add_filter( 'script_loader_src', 'wpsb_remove_css_js', 9999 );
 
/*========== speedup wp site Clean Head ==========*/
function wpsb_start_cleanhead() {
  add_action('init', 'wpsb_clean_head');
} 
function wpsb_clean_head() {
  remove_action( 'wp_head', 'rsd_link' );
  remove_action( 'wp_head', 'feed_links_extra', 3 );
  remove_action( 'wp_head', 'feed_links', 2 );
  remove_action( 'wp_head', 'wlwmanifest_link' );
  remove_action( 'wp_head', 'index_rel_link' );
  remove_action( 'wp_head', 'parent_post_rel_link', 10, 0 );
  remove_action( 'wp_head', 'start_post_rel_link', 10, 0 );
  remove_action('wp_head', 'rel_canonical', 10, 0 );
  remove_action( 'wp_head', 'wp_shortlink_wp_head', 10, 0 );
  remove_action( 'wp_head', 'adjacent_posts_rel_link_wp_head', 10, 0 );
  remove_action( 'wp_head', 'wp_generator' );
}
add_action('after_setup_theme','wpsb_start_cleanhead');
/*=========== speedup wp site Frontend Scripts =============*/
function wpsb_frontend_scripts() {    
   wp_enqueue_script('wpsb-frontend-custom', plugins_url( '/js/wpsb-frontend-custom.js' , __FILE__ ), array('jquery'),null,true);
}
 add_action( 'wp_enqueue_scripts', 'wpsb_frontend_scripts' );  